# deploy-enc-arch.sh

This script will allow a fast deployment of an
[Arch Linux](https://archlinux.org) server with:

## Features

- NTP enabled
- LVM on LUKS with:
  - An optional swap partition depending on installed RAM when the script is
    run
  - An ext4 root partition using the remaining of the disk
- Pacman mirrolist with french servers
- Europe/Paris as timezone
- fr_FR.UTF-8 and en_US.UTF-8 locales
- A fr-latin1 keymap
- Systemd resolved as DNS resolver
- Systemd networkd with DHCP on eth0 (with interface renaming disabled)
- A default privileged local admin
- Grub + dropbear SSH to unlock server remotely on port 222
- OpenSSH with root login and password authentication disabled

## Usage

After booting on archiso, copy `config` and `deploy.sh` on the host to deploy.
Edit `config` and replace *ADMIN_NAME*, *PASSPHRASE* and *AUTHORIZED_KEY* if
required.

Then run `bash deploy.sh -h HOSTNAME -d /path/to/device` or
`bash deploy.sh -h HOSTNAME -d /path/to/device -s` if you don't want a swap
partition.

The script will detect if archiso was started in legacy or UEFI mode.
If UEFI is available, Arch Linux will be installed on a UEFI partition.

## Limitations

As I use this script to deploy my Arch Linux hosts before running Ansible
playbooks, this script doesn't allow the creation of multiple users nor the
addition of multiple SSH keys.
